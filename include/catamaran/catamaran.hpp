#if !defined(THRUSTERS_HPP)
#define THRUSTERS_HPP

#include <crono>
#include <cinttypes>


using PwmChannel = unsigned int;
using PigpiodHandle = int;


class Thrusters {

	public:

		Thrusters(PigpiodHandle pi);

		void step()
		void request_thrust(double left, double right);

	private:
		PigpiodHandle pi;

		static constexpr int PWM_STOP = 1500;
    		// PWM_MAX_DELTA and PWM_WAIT together describe the rate limit.
    		static constexpr int PWM_MAX_DELTA = 5; // PWM
    		static constexpr int PWM_WAIT = 100; // milliseconds
    		static constexpr int PWM_MIN = 1400;
    		static constexpr int PWM_MAX = 1600;
		







};
#endif
