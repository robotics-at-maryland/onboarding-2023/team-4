#pragma once

#if !defined(THRUSTERS_HPP)
#define THRUSTERS_HPP

#include <chrono>
#include <cinttypes>
#include <iostream>


using PwmChannel = unsigned int;
using PigpiodHandle = int;

class Thrusters {
public:
    Thrusters(PigpiodHandle pi);

    void step();

    // left and right are percentages, not the actual PWM values
    // left and right should be values within [-100, 100]
    void request_thrust(double left, double right);

private:
    // Returned by `pigpio_start()`.
    PigpiodHandle pi;

    // 1500 microseconds means stop/zero thrust.
    static constexpr int PWM_STOP = 1500;
    // PWM_MAX_DELTA and PWM_WAIT together describe the rate limit.
    static constexpr int PWM_MAX_DELTA = 5; // PWM
    static constexpr int PWM_WAIT = 100; // milliseconds
    static constexpr int PWM_MIN = 1400;
    static constexpr int PWM_MAX = 1600;

    // current left and right PWM values
    public: int leftThrust, rightThrust; private:

    // target left and right PWM value
    int leftTargetThrust, rightTargetThrust;

    // time is in seconds
    std::chrono::time_point<std::chrono::high_resolution_clock> currentTime; 
    std::chrono::time_point<std::chrono::high_resolution_clock> timeSinceLastRequest; 
    std::chrono::time_point<std::chrono::high_resolution_clock> timeSinceLastStepCall; 
    std::chrono::duration<double> requestThrustDuration; 
    std::chrono::duration<double> stepCallDuration;
    
    double clamp(double val, double min, double max);
    double convertPercentToPWM(double percent);
    int abs(int val);
};

#endif
