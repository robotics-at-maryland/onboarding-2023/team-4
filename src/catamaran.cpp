#include <iostream>
#include <sstream>
#include "thrusters.hpp"
using std::cerr;


Thrusters thrusters = Thrusters(1500);


void parse()
{
    double leftThrust, rightThrust;
    std::string line;
    std::string command;

    while (std::getline(std::cin, line)) {
        std::istringstream iss(line);

        iss >> command;

        if (command == "request-thrust") {
            iss >> leftThrust >> rightThrust;
            thrusters.request_thrust(leftThrust,rightThrust);
            if (iss.fail()) {
                cerr << "error: Failed to parse thrust values.\n";
            } else {
                cerr << '\n';
            }
        }
    }
}


int main()
{
    bool running = true;

    while(running){

        parse();
        thrusters.step();



    }
    return 0;





}

