#include "thrusters.hpp"

Thrusters::Thrusters(PigpiodHandle pi)
{
    this->pi = pi;
    leftThrust = PWM_STOP;
    rightThrust = PWM_STOP;
    leftTargetThrust = PWM_STOP;
    rightTargetThrust = PWM_STOP;

    timeSinceLastRequest = std::chrono::high_resolution_clock::now();
    timeSinceLastStepCall= std::chrono::high_resolution_clock::now();
}

void Thrusters::step()
{
    int leftPWMChange = leftTargetThrust - leftThrust;
    int rightPWMChange = rightTargetThrust - rightThrust;

    currentTime = std::chrono::high_resolution_clock::now();
    stepCallDuration = currentTime - timeSinceLastStepCall;

    leftThrust += clamp(leftPWMChange, -50*stepCallDuration.count(), 50*stepCallDuration.count());
    rightThrust += clamp(rightPWMChange, -50*stepCallDuration.count(), 50*stepCallDuration.count());

    leftThrust = clamp(leftThrust, PWM_MIN, PWM_MAX);
    rightThrust = clamp(rightThrust, PWM_MIN, PWM_MAX);

    timeSinceLastStepCall = std::chrono::high_resolution_clock::now();
    std::cout << leftThrust << " " << rightThrust << std::endl;
    
}

void Thrusters::request_thrust(double left, double right)
{
    // if less than 100ms has passed since last request, ignore the current request
    currentTime = std::chrono::high_resolution_clock::now();
    requestThrustDuration = currentTime - timeSinceLastRequest;
    if (requestThrustDuration.count() < PWM_WAIT/1000)
        return;

    left = clamp(left, -100, 100);
    right = clamp(right, -100, 100);

    double leftPWM = convertPercentToPWM(left);
    double rightPWM = convertPercentToPWM(right);

    leftTargetThrust = leftPWM;
    rightTargetThrust = rightPWM;
    timeSinceLastRequest = std::chrono::high_resolution_clock::now();
}

double Thrusters::clamp(double val, double min, double max)
{
    if (val < min) { return min; }
    if (val > max) { return max; }
    return val;
}

double Thrusters::convertPercentToPWM(double percent)
{
    double pwmRange = PWM_MAX - PWM_MIN;
    double percentAsDecimal = percent / 100;
    double pwmValue = PWM_STOP + ( percentAsDecimal * (pwmRange / 2) );

    return pwmValue;
}

int Thrusters::abs(int val)
{
    if (val < 0) { return val *= -1; }
    return val;
}